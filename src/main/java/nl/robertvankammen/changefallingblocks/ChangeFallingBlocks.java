package nl.robertvankammen.changefallingblocks;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDropItemEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Random;

import static org.bukkit.entity.EntityType.FALLING_BLOCK;

public final class ChangeFallingBlocks extends JavaPlugin implements Listener {
    private final Random random = new Random();

    @Override
    public void onEnable() {
        this.getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    /**
     * what this methode does is check if the item that is dropped is a falling block and if is a gravel item
     * then it has a 25% to cancel it (so it does not drop the item)
     *
     * So if you place a torch under a gravel tower so it brakes on the torch then it has a 75% droprate
     */
    @EventHandler
    public void onDrop(EntityDropItemEvent event){
        if (event.getEntityType() == FALLING_BLOCK){
            if (event.getItemDrop().getItemStack().getType().equals(Material.GRAVEL)){
                var randomValue = random.nextInt(4);
                if (randomValue == 1) {
                    event.setCancelled(true);
                    System.out.println("Canceld");
                } else {
                    System.out.println(randomValue);
                }
            }
        }
    }
}
